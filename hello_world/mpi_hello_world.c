#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
  //Initialise the MPI communicator
  MPI_Init(&argc, &argv);

  //Find out size
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  //Find out rank
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  printf("Hello world from rank %d out of %d processors\n",
         world_rank, world_size);
  
  //Terminate MPI
  MPI_Finalize();
}
