from mpi4py import MPI

# make reference to the communicator
MPI_COMM_WORLD = MPI.COMM_WORLD

#Find out size
world_size = MPI_COMM_WORLD.Get_size()

#Find out rank
world_rank = MPI_COMM_WORLD.Get_rank()

print("Hello world from rank %d out of %d processors"%(world_rank, world_size))
