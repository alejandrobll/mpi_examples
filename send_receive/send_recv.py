from mpi4py import MPI

def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    
    if(size < 2):
        print("Error: Number of cores must be greater than 1.")
        return
    
    if(rank == 0):
        number = -1
        comm.send(number, dest=1, tag=10)
    elif(rank == 1):
        number = comm.recv(source=0, tag=10)
        
        print("Process 1 received number %d from process 0"% number)
        
    return
    
if __name__ == '__main__': 
    main()
