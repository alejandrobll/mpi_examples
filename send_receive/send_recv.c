#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  // Initialize the MPI environment
  MPI_Init(NULL, NULL);
  
  // Find out rank
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Find out size
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // We assume at least 2 processes for this task
  if (world_size < 2) {
    fprintf(stderr, "World size must be greater than 1 for %s\n", argv[0]);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  
  int number;  
  if (world_rank == 0) {
    
    // Rank 0 defines number = 1 and sends it to rank 1
    number = -1;
    //MPI_Send(&number, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
    
  } else if (world_rank == 1) {

    //If rank=1, receive the message from rank=0
    MPI_Recv(&number, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("Process 1 received number %d from process 0\n", number);
    
  }
  
  //Terminate MPI
  MPI_Finalize();
}
