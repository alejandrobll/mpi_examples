
// This file is part of the HPC workshop of Durham University 
// Prepared by Alejandro Benitez-Llambay, November 2018 
// email: alejandro.b.llambay@durham.ac.uk 

#ifdef _OPENMP
#include <omp.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include "proto.h"
#include <time.h>



void main(int argc, char *argv[]){
  struct Image myimage;
  int n_threads;
  double cpu_time;
  clock_t start, end;
  struct Image output;

#ifdef _OPENMP
#pragma omp parallel
  {
    n_threads = omp_get_num_threads();
  }
  printf("number of threads=%d \n", n_threads);
#else
  printf("Serial version\n");
#endif
 
  
  read_ppm(argv[1], &myimage);

  int n = 10;
  
  output = blur_mean_automatic(myimage, n);
  
  write_ppm(argv[2], output);
}
