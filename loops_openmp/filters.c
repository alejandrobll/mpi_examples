
// This file is part of the HPC workshop of Durham University 
// Prepared by Alejandro Benitez-Llambay, November 2018 
// email: alejandro.b.llambay@durham.ac.uk 

#include <stdlib.h>
#include <stdio.h>
#include "proto.h"
#include "math.h"
#include <time.h>

#ifdef _OPENMP
#include <omp.h>  /* use OpenMP only if needed */
#endif

/*blur filter: we use a square window function with equal weights 

                 |1|1|1|
Output = 1/9 *   |1|1|1|  * Image
                 |1|1|1|
               
*/

#ifdef _OPENMP

struct Image blur_mean_manual(struct Image input, int n){
  struct Image output;
  int i,j,k,l;
  int dimx, dimy;
  double start, end;

  printf(GRN "Applying mean blur filter... \n " RESET);
  fflush(stdout);

  /* We define the output Image */
  //-----------------------------//
  dimx = input.dimx;
  dimy = input.dimy;

  output.r = (float *)malloc(sizeof(float)*input.dimx*input.dimy);
  output.g = (float *)malloc(sizeof(float)*input.dimx*input.dimy);
  output.b = (float *)malloc(sizeof(float)*input.dimx*input.dimy);

  output.dimx = dimx;
  output.dimy = dimy;

  float npixels = pow(2*n+1.0, 2.0);
  
  //initialize the output
  for (i = 0; i < dimx; i++){
    for (j = 0; j < dimy; j++){
      output.r[i+dimx*j] = 0.0;
      output.g[i+dimx*j] = 0.0;
      output.b[i+dimx*j] = 0.0;
      
    }
  }


  start = omp_get_wtime();

#pragma omp parallel
 {
   
   int nthreads  = omp_get_num_threads();
   int thread_id = omp_get_thread_num();
   int pixels_per_thread = dimx * dimy / nthreads;
   
   for(int ll=0; ll<pixels_per_thread; ll++){
     
     int id = thread_id + nthreads * ll;
     int i = input.xcoord[id];
     int j = input.ycoord[id];
     
     for(int k=-n; k<=n; k++){
       for(int l=-n; l<=n; l++){
	 
	 int idx = i+k;
	 int idy = j+l;
	 
	 if(idx < 0) idx = i+k+dimx;
	 if(idx >= dimx) idx = i+k-dimx;  
	 
	 if(idy < 0) idy = j+l+dimy;
	 if(idy >= dimy) idy = j+l-dimy;
	 
	 int id_out = idx + dimx * idy;
	 
	 output.r[id] += input.r[id_out] * 1.0/npixels;
	 output.g[id] += input.g[id_out] * 1.0/npixels;
	 output.b[id] += input.b[id_out] * 1.0/npixels; 
       }
     }
   }
   
 } // end of parallel section
 
 //---------------------------//
 end = omp_get_wtime();

 printf(MAG "manual loop took:%6f\n" RESET , end-start);

 printf(BLU "Done \n" RESET);
  
 return output;
}
#endif


struct Image blur_mean_automatic(struct Image input, int n){
  struct Image output;
  int i,j,k,l;
  int dimx, dimy;

  printf(GRN "Applying mean blur filter... \n " RESET);
  fflush(stdout);

  /* We define the output Image */
  //-----------------------------//
  dimx = input.dimx;
  dimy = input.dimy;

  output.r = (float *)malloc(sizeof(float)*dimx*dimy);
  output.g = (float *)malloc(sizeof(float)*dimx*dimy);
  output.b = (float *)malloc(sizeof(float)*dimx*dimy);

  output.dimx = dimx;
  output.dimy = dimy;

  float npixels = pow(2*n+1.0, 2.0);
  
  //initialize the output
  for (i = 0; i < dimx*dimy; i++){
      output.r[i] = 0.0;
      output.g[i] = 0.0;
      output.b[i] = 0.0;
  }
  //-----------------------------//
  
  //Automatic loop//
  //-----------------------------//
  
#ifdef _OPENMP
  double start = omp_get_wtime();
#else
  clock_t start = clock();
#endif
  
#pragma omp parallel for private(i,j)
  for(int id=0; id<dimx*dimy; id++){
    
    i = input.xcoord[id];
    j = input.ycoord[id];
    
    for(int k=-n; k<=n; k++){
      int idx = i+k;
      if(idx < 0) idx = i+k+dimx;
      if(idx >= dimx) idx = i+k-dimx;
     
      for(int l=-n; l<=n; l++){
	int idy = j+l;
	
	if(idy < 0) idy = j+l+dimy;
	if(idy >= dimy) idy = j+l-dimy;
	
	int id_out = idx + dimx * idy;
	
	
	output.r[id] += input.r[id_out]* 1.0/npixels;
	output.g[id] += input.g[id_out]* 1.0/npixels;
	output.b[id] += input.b[id_out]* 1.0/npixels; 
      }
    }
  }
  
  //---------------------------//
#ifdef _OPENMP
  double end = omp_get_wtime();
#else
  clock_t end = clock();
#endif
  
#ifdef _OPENMP
  printf(MAG "Automatic loop took:%6f\n" RESET , end-start);
#else
  printf(MAG "Automatic loop took:%6f\n" RESET , ((double)end - start)/CLOCKS_PER_SEC);
#endif
	 
  printf(BLU "Done \n" RESET);
  
  return output;
}
